library(data.table)
library(lulu)

########## arguments
args <- commandArgs(TRUE)
table_file <- args[1]
sim_file <- args[2]
fasta_file <- args[3]

###########
asv_table <- fread(table_file)
asv_table_df <- data.frame(asv_table,
                           row.names = "amplicon",
                           check.names = FALSE)

OTU_sim <- fread(sim_file, sep="\t") |>
    data.frame()

ctk <- setdiff(names(asv_table_df), c( "sequence", "total", "spread"))

lulu_res <- lulu(asv_table_df[,ctk], OTU_sim)

lulu_res_table <- lulu_res$curated_table |>
    data.table(keep.rownames = T)

setnames(lulu_res_table, "rn", "amplicon")

asv_table <- merge(asv_table[,.(amplicon, sequence, total, spread)], lulu_res_table, by = "amplicon")

x <- rowSums(asv_table[,.SD,.SDcols=ctk])
asv_table[,total:=x]
x <- apply(asv_table[,.SD,.SDcols=ctk],1,function(X) sum(X>0))
asv_table[,spread:=x]

asv_table <- asv_table[order(total, decreasing = TRUE)]


fwrite(asv_table,
       file = sub("tsv.gz", "lulu.tsv.gz", table_file),
       sep = "\t",
       quote = FALSE)

seqinr::write.fasta(sequences = as.list(asv_table[,sequence]),
		    names = asv_table[,amplicon],
		    file.out = sub("fasta.gz", "lulu.fasta", fasta_file))
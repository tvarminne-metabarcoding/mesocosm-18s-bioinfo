#!/bin/bash

source ${CONFIG}

Rscript scripts/R/dada_table.R $PROJECT $VERSION

sleep 1

gzip "outputs/asv_table/"$PROJECT"_dada2_"$VERSION".fasta"
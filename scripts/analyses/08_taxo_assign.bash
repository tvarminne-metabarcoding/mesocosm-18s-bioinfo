#!/bin/bash
#SBATCH --partition long
#SBATCH --mem 20GB
#SBATCH --cpus-per-task 16

source $CONFIG

Rscript scripts/R/idtaxa_assign.R $IDTAXA_INPUT $SLURM_CPUS_PER_TASK $IDTAXA_REFDB $IDTAXA_OUTPUT

bash scripts/bash/format_for_vsearch.bash archives/refdb/pr2_version_5.0.0_SSU_UTAX.fasta.gz ';tax=' ','

INPUT="outputs/asv_table/mesocosm-18s_dada2_v1.0.fasta.gz"
OUTPUT=$(echo $INPUT | sed -r 's/\.fasta\.gz//')

bash scripts/bash/vsearch_LCA.bash ${CONFIG} ${INPUT} ${OUTPUT}

zcat ${OUTPUT}".vsearch.out.lca.gz" | \
    awk '
        BEGIN{print "amplicon","upper_similarity_vsearch","lower_similarity_vsearch","references_vsearch","taxonomy_vsearch"}
        {gsub(/(;\*)+/,""); print}
    ' > tmp/vsearch_lca.out

Rscript scripts/R/get_asv_table_with_taxo.R


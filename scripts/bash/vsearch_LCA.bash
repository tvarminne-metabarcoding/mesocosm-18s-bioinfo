#!/bin/bash

source $1
INPUT=$2
OUTPUT=$3

TMP=$(mktemp --tmpdir="tmp/")

# look for hits
vsearch --usearch_global ${INPUT} \
            -db ${VSEARCH_REFDB} \
            --id 0.80 \
            --maxaccepts 0 \
	        --userout $TMP \
	        --threads $SLURM_CPUS_PER_TASK \
            --query_cov 0.97 \
            --iddef 2 \
            --userfields "query+id2+target"
	    
awk -v threshold=$VSEARCH_THRESHOLD -f scripts/awk/LCA.awk $VSEARCH_TAXO $TMP | \
	gzip > ${OUTPUT}".vsearch.out.lca.gz"

awk -v threshold=100 -f scripts/awk/LCA.awk $VSEARCH_TAXO $TMP | \
    awk '{print $1,$2,$4,$5}' | \
	gzip > ${OUTPUT}".vsearch.out.besthit.gz"

rm -f $TMP

#!/bin/bash

OUTPUT_DIR="outputs/refdb"

OUTPUT_TAXO=$(echo $1 | \
    awk -v outdir=$OUTPUT_DIR '
        BEGIN{FS="/"}
        {sub(/.fas(ta)*(\.gz)*/,"_taxo.tsv",$NF); print outdir"/"$NF}
    ')

OUTPUT_SEQ=$(echo $OUTPUT_TAXO | sed 's/_taxo.tsv/_vsearch.fasta/')

zcat -f "$1" | \
        awk -v sep="$2" \
            -v taxosep="$3" \
            -v outtaxo="$OUTPUT_TAXO" \
            -v outseq="$OUTPUT_SEQ" \
            -f scripts/awk/ref_db_format.awk

gzip -f "$OUTPUT_SEQ"

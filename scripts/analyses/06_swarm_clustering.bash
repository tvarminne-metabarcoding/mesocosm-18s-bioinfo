#!/bin/bash

module load r/4.1.1
export TMPDIR=tmp/
module load vsearch/2.18.0
module load swarm/3.1.0

ASV_TABLE="outputs/asv_table/mesocosm-coi_dada2_v1.0.filtered.table.tsv.gz"
FASTA_FOR_SWARM=$(mktemp --tmpdir="tmp/")
TMP_REPRESENTATIVES=$(mktemp --tmpdir="tmp/")

zcat $ASV_TABLE | \
    awk 'NR > 1 {print ">"$1"_"$3"\n"$2}' > $FASTA_FOR_SWARM

swarm \
    -d 4 -t 1 \
    -i ${ASV_TABLE/.tsv.gz/_4f.struct} \
    -s ${ASV_TABLE/.tsv.gz/_4f.stats} \
    -w ${ASV_TABLE/.tsv.gz/_4f_representatives.fas} \
    -o ${ASV_TABLE/.tsv.gz/_4f.swarms} \
    ${FASTA_FOR_SWARM}

Rscript scripts/R/swarm_merge.R
#!/bin/bash
#SBATCH --mem 10GB

source ${CONFIG}

OUTPUT="tmp/lulu.tmp"
THREADS=1

vsearch --usearch_global ${FASTA} \
    --db ${FASTA} \
    --self \
    --id .84 \
    --iddef 0 \
    --userout ${OUTPUT} \
    -userfields "query+target+id" \
    --maxaccepts 0 \
    --threads ${THREADS} \
    --maxhits 10

Rscript scripts/R/lulu.R $ASV_TABLE $OUTPUT $FASTA
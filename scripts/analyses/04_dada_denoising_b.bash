#!/bin/bash
#
#SBATCH --partition fast

source $CONFIG

RUN=$(awk "NR==$SLURM_ARRAY_TASK_ID" $MANIFEST)

Rscript scripts/R/dada_denoising.R $RUN $THREADS $CONCAT

#!/bin/bash

source $1

# Define primers and output files
MIN_F=$(( ${#PRIMER_F} * 2 / 3 ))
MIN_R=$(( ${#ANTI_PRIMER_R} * 2 / 3 ))

# Trim forward & reverse primers, format
OUTPUT=${INPUT/.fasta.gz/_${AMPLICON}_${PRIMER_F}_${ANTI_PRIMER_R}.fasta.gz}
LOG=${INPUT/.fasta.gz/_${AMPLICON}_${PRIMER_F}_${ANTI_PRIMER_R}.log}

zcat "${INPUT}" | \
     cutadapt --discard-untrimmed -e $ERROR -g "${PRIMER_F}" -O "${MIN_F}" - 2> "${LOG}" | \
     cutadapt -e $ERROR --minimum-length ${MIN_LENGTH} -a "${ANTI_PRIMER_R}" -O "${MIN_R}" - 2>> "${LOG}" | \
     gzip -f > "${OUTPUT}"

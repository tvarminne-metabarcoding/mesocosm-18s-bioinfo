#!/bin/bash
#SBATCH --partition long

###################################################################
# script name   : make.bash
# description   : This script produces an ASV table using dada2
# usage         : sbatch make.sh but better to run the lines one
#                 by one and check manualy the outputs
# author    : Nicolas Henry
# contact    : nicolas.henry@cnrs.fr
###################################################################

# Clear out working directory

rm -r outputs log tmp manifests slurm*

# Outputs, log files and
# temporary files folders are created

mkdir -p \
    archives/raw \
    archives/refdb \
    outputs/reads/fwd/dada_filtered \
    outputs/reads/rev/dada_filtered \
    outputs/taxo_assignment \
    outputs/asv_table \
    outputs/quality_plot \
    outputs/error_plot \
    outputs/refdb \
    log \
      tmp \
    manifests

############################################
# Primer trimming
############################################

FASTQ_FOLDER="archives/raw"

# Create a manifest file listing
# the files (fastqs) to work with
# It contains four columns:
# 1: sample name ; 2: run name ; 3: forward file ; 4: reverse file

ls $FASTQ_FOLDER | \
    paste - - | \
    awk -F "\t" '
        !/pcr-0ctrl/{
            split($1, sample_info, "-")
            sub(/_.+$/,"",sample_info[9])
            print sample_info[5]"-"sample_info[3]"-"sample_info[4],sample_info[9], "archives/raw/"$1, "archives/raw/"$2
        }
        /pcr-0ctrl/{
            split($1, sample_info, "-")
            sub(/_.+$/,"",sample_info[8])
            print sample_info[3]"-"sample_info[4],sample_info[8], "archives/raw/"$1, "archives/raw/"$2
        }
    ' > manifests/mesocosm-18s_primer_trimming.txt

# Remove the primers
# parameters can be directly edited
# in config/toy-dataset_primer_trimming.cfg

job_id_trimming=$(sbatch \
    --export=CONFIG="config/mesocosm-18s_primer_trimming.cfg" \
    scripts/analyses/01_primer_trimming_a.bash | awk '{print $4}') 

############################################
# Quality assessment
############################################

# Generate quality plots
sbatch --dependency=afterany:$job_id_trimming \
    --export=CONFIG="config/mesocosm-18s_quality_plot.cfg" \
    scripts/analyses/02_quality_plot.bash

############################################
# Dada2 filtering
############################################

# run filtering
job_id_filtering=$(sbatch --dependency=afterany:$job_id_trimming \
    --export=CONFIG="config/mesocosm-18s_dada_filtering.cfg" \
    scripts/analyses/03_dada_filtering_a.bash | awk '{print $4}')

############################################
# Dada2 denoising
############################################

job_id_denoising=$(sbatch --dependency=afterany:$job_id_filtering \
    --export=CONFIG="config/mesocosm-18s_dada_denoising.cfg" \
    scripts/analyses/04_dada_denoising_a.bash | awk '{print $4}')

############################################
# ASV table assembly
############################################

job_id_table=$(sbatch --dependency=afterany:$job_id_denoising \
    --export=CONFIG="config/mesocosm-18s_dada_table.cfg" \
    scripts/analyses/05_dada_table.bash | awk '{print $4}')

############################################
# lulu
############################################

job_id_lulu=$(sbatch --dependency=afterany:$job_id_swarm \
    --export=CONFIG="config/mesocosm-18s_lulu.cfg" \
    scripts/analyses/07_lulu.bash | awk '{print $4}')

############################################
# taxo assign
############################################

job_id_taxo=$(sbatch --dependency=afterany:$job_id_lulu \
    --export=CONFIG="config/mesocosm-18s_assign.cfg" \
    scripts/analyses/08_taxo_assign.bash | awk '{print $4}')

############################################
# extract V9 from PR2
############################################

sbatch --dependency=afterany:$job_id_taxo \
    --export=CONFIG="config/pr2_trim.cfg" \
    scripts/analyses/09_refdb_trim.bash